package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row >= 0 && row < grid.size()) {
            grid.remove(row);
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (grid.size() == 0) {
            return true;
        }
    
        int targetSum = grid.get(0).stream().mapToInt(Integer::intValue).sum();
    
        // Check row sums
        for (ArrayList<Integer> row : grid) {
            int sum = row.stream().mapToInt(Integer::intValue).sum();
            if (sum != targetSum) {
                return false;
            }
        }
    
        // Check column sums
        int numCols = grid.get(0).size();
        for (int col = 0; col < numCols; col++) {
            int colSum = 0;
            for (ArrayList<Integer> row : grid) {
                if (col < row.size()) {
                    colSum += row.get(col);
                } else {
                    return false; // Inconsistent column size
                }
            }
            if (colSum != targetSum) {
                return false;
            }
        }
    
        return true;
    }

}