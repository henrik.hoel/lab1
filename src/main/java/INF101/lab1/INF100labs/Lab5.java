package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        // Testing multipliedWithTwo
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> multipliedList1 = multipliedWithTwo(list1);
        System.out.println("multipliedWithTwo list1: " + multipliedList1);

        // Testing removeThrees
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> removedList1 = removeThrees(list2);
        System.out.println("removeThrees list2: " + removedList1);

        // Testing uniqueValues
        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> uniqueList1 = uniqueValues(list3);
        System.out.println("uniqueValues list3: " + uniqueList1);
        
        // Testing addList
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a, b);
        System.out.println("addList a + b: " + a);
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> resultList = new ArrayList<>();
        for (Integer value : list) {
            resultList.add(value * 2);
        }
        return resultList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> resultList = new ArrayList<>();
        for (Integer value : list) {
            if (value != 3) {
                resultList.add(value);
            }
        }
        return resultList;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> resultList = new ArrayList<>();
        for (Integer value : list) {
            if (!resultList.contains(value)) {
                resultList.add(value);
            }
        }
        return resultList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }
} 