package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("ark", "diablo", "minecraft");
        findLongestWords("eple", "banan", "ananas");
        findLongestWords("fire", "fem", "seks");

        System.out.println(isLeapYear(2022)); 
        System.out.println(isLeapYear(1996)); 
        System.out.println(isLeapYear(1900)); 
        System.out.println(isLeapYear(2000)); 

        System.out.println(isEvenPositiveInt(123456)); 
        System.out.println(isEvenPositiveInt(-2)); 
        System.out.println(isEvenPositiveInt(123)); 
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLength = Math.max(word1.length(), Math.max(word2.length(), word3.length()));

        if (word1.length() == maxLength) System.out.println(word1);
        if (word2.length() == maxLength) System.out.println(word2);
        if (word3.length() == maxLength) System.out.println(word3);
    }

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
    }

    public static boolean isEvenPositiveInt(int num) {
        return num > 0 && num % 2 == 0;
    }

}
